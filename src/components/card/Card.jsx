import React from "react";
import PropTypes from "prop-types";

import "./card.css";

export const Card = ({ cardNumber,cardColor }) => {
  return (
    <div className="card-container" style={{ backgroundColor: cardColor }}>
      {cardNumber}
    </div>
  );
}; 

Card.propTypes = {
  cardNumber: PropTypes.number.isRequired,
  cardColor: PropTypes.string.isRequired
}