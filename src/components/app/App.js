import { Slider } from "../slider/Slider";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header>
        <h1 className="app-header">Ряд чисел Фибоначчи</h1>
      </header>
      <Slider />
    </div>
  );
}

export default App;
