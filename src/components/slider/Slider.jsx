import React, { useState } from "react";
import { Card } from "../card/Card";
import { DEFAULT_CARDS, COLOR_PALETTE } from "../../utils/consts.js";

import "./slider.css";

export const Slider = () => {
  const [fibonacciCards, setFibonacciCards] = useState(DEFAULT_CARDS);
  const [ofsetX, setOfsetX] = useState(0);

  useState(() => {
    let cardsStore = JSON.parse(localStorage.getItem("cardsInfo"));
    let offsetStore = JSON.parse(localStorage.getItem("ofsetX"));

    if (cardsStore?.length > fibonacciCards.length) {
      setFibonacciCards(cardsStore);
      setOfsetX(offsetStore);
    }
  }, []);

  const handleClickNext = () => {
    const newCard = {
      id: fibonacciCards[fibonacciCards.length - 1].id + 1,
      value:
        fibonacciCards[fibonacciCards.length - 1].value +
        fibonacciCards[fibonacciCards.length - 2].value,
      color: COLOR_PALETTE[Math.floor(Math.random() * 10)],
    };

    setFibonacciCards((prevState) => [...prevState, newCard]);
    localStorage.setItem(
      "cardsInfo",
      JSON.stringify([...fibonacciCards, newCard])
    );

    setOfsetX((prevOfset) => prevOfset + 220);
    localStorage.setItem("ofsetX", JSON.stringify(ofsetX + 220));
  };

  const handleClickPrev = () => {
    if (ofsetX > 0) {
      setFibonacciCards((prevState) => {
        prevState.pop();
        localStorage.setItem("cardsInfo", JSON.stringify(prevState));
        return prevState;
      });

      setOfsetX((prevOfset) => prevOfset - 220);
      localStorage.setItem("ofsetX", JSON.stringify(ofsetX - 220));
    }
  };

  return (
    <main className="sliders-container">
      <div className="current-slides-container">
        <div
          className="all-slides-container"
          style={{ transform: `translateX(-${ofsetX}px)` }}
        >
          {fibonacciCards.map((card) => (
            <Card
              key={card.id}
              cardNumber={card.value}
              cardColor={card.color}
            />
          ))}
        </div>
      </div>
      <button className="button-right" onClick={handleClickNext}></button>
      <button className="button-left" onClick={handleClickPrev}></button>
    </main>
  );
};
