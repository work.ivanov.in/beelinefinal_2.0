export const DEFAULT_CARDS = [
  {
    id: 1,
    value: 0,
    color: "hsla(79, 83%, 77%, 1)",
  },
  {
    id: 2,
    value: 1,
    color: "hsla(55, 98%, 84%, 1)",
  },
  {
    id: 3,
    value: 1,
    color: "hsla(337, 92%, 90%, 1)",
  },
];

export const COLOR_PALETTE = [
  "hsla(17, 100%, 50%, 1)",
  "hsla(45, 100%, 50%, 1)",
  "hsla(71, 100%, 50%, 1)",
  "hsla(100, 100%, 50%, 1)",
  "hsla(158, 100%, 50%, 1)",
  "hsla(207, 100%, 50%, 1)",
  "hsla(246, 100%, 50%, 1)",
  "hsla(278, 100%, 50%, 1)",
  "hsla(306, 100%, 50%, 1)",
  "hsla(352, 100%, 50%, 1)",
];
